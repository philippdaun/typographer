# Typographer for ProcessWire

Currently in stable-beta, Typographer is a ProcessWire wrapper for the awesome PHP Typography class, originally authored by KINGdesk LLC and enhanced by Peter Putzer for wp-Typography. Like Smartypants, it supercharges text fields with enhanced typography and typesetting, such as smart quotations, hyphenation in 59 languages, ellipses, copyright-, trade-, and service-marks, math symbols, and more.

The textformatter is configurable. Feel free to visit its configuration page to see what can be toggled and configured. As the module is in alpha, there are plans to include more of the settings that are made available by PHP Typography. There are also plans to refactor a little bit, making use of a more automated approach and some proper autoloading, traits, etc. (this is a practice I’m implementing in all my modules as thay are upgraded).

## Licenses

The textformatter is licensed under MIT, and the libraries it depends on have their own licenses. Please review these in the [license file](LICENSE.md).